import numpy as np
import matplotlib.pyplot as plt

from lib import draw_rect, draw_ellipse

beam = plt.imread('beam.png')

circle = draw_ellipse((0, 0), 4, 4)
ellipseh1 = draw_ellipse((0, 0), 6, 4)
ellipseh2 = draw_ellipse((0, 0), 10, 4)
ellipsev1 = draw_ellipse((0, 0), 4, 6)
ellipsev2 = draw_ellipse((0, 0), 4, 10)
square = draw_rect((-2, -2), 4, 4)
recth1 = draw_rect((-3, -2), 6, 4)
recth2 = draw_rect((-5, -2), 10, 4)
rectv1 = draw_rect((-2, -3), 4, 6)
rectv2 = draw_rect((-2, -5), 4, 10)


plt.imsave('output_targets/circle.png', circle)
plt.imsave('output_targets/ellipseh1.png', ellipseh1)
plt.imsave('output_targets/ellipseh2.png', ellipseh2)
plt.imsave('output_targets/ellipsev1.png', ellipsev1)
plt.imsave('output_targets/ellipsev2.png', ellipsev2)
plt.imsave('output_targets/square.png', square)
plt.imsave('output_targets/recth1.png', recth1)
plt.imsave('output_targets/recth2.png', recth2)
plt.imsave('output_targets/rectv1.png', rectv1)
plt.imsave('output_targets/rectv2.png', rectv2)
