import os
import sys
import numpy as np
import matplotlib.pyplot as plt

from glob import glob

hset = sys.argv[1]
label = sys.argv[2]

filepath = os.path.join('..', hset, 'bottleneck', label, '*.txt')

fig, axes = plt.subplots(nrows = 5, ncols = 8, figsize = (20, 20))
image_base = np.zeros((32, 64))
for i, filename in enumerate(glob(filepath)):
  image_str = ''
  with open(filename) as f: image_str = f.read()
  image = np.array(image_str.split(','), dtype=np.float)
  image = image.reshape(32, 64)
  image -= image_base
  if i == 0:
      image_base = image
  axes[i // 8, i % 8].axis('off')
  axes[i // 8, i % 8].imshow(image)
plt.subplots_adjust(wspace=.05, hspace=.005)
plt.show()
