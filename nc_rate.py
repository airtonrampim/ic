import numpy as np
import matplotlib.pyplot as plt
import hologram as h

from lib import perturb, read_file

beam = plt.imread('beam.png')
label = read_file('noise_output/label.txt')
target_original = plt.imread('noise_output/targets/%s_original.jpg' % label)

#26, 51, 102, 153, 204, 255
#Rates = np.array([0.001, 0.005, 0.01, 0.05, 0.1])
Rates = np.arange(0, 0.101, 0.005)
Rates[0] = 0.001

for rate in Rates:
    print('Processando a armadilha para o erro de %.1f %% ...' % (rate*1E2))
    
    target_perturbed = perturb(target_original, rate, 255)
    hologram_perturbed, intensity_perturbed = h.process(beam, target_perturbed)

    plt.imsave('noise_output/targets/%s_rate_%.3f.jpg' % (label, rate), target_perturbed)
    plt.imsave('noise_output/holograms/%s_rate_%.3f.jpg' % (label, rate), hologram_perturbed)
    plt.imsave('noise_output/intensities/%s_rate_%.3f.jpg' % (label, rate), intensity_perturbed)
