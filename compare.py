#!/home/airton/.local/anaconda3/bin/python

import sys
import numpy as np
import matplotlib.pyplot as plt

image1 = plt.imread(sys.argv[1])
image2 = plt.imread(sys.argv[2])

if np.ndim(image1) > 2: image1 = image1[:, :, 0]
if np.ndim(image2) > 2: image2 = image2[:, :, 0]

image_dif = image1 - image2

plt.figure()
plt.title(sys.argv[3])
plt.axis("off")
plt.imshow(image_dif, vmin=0, vmax=255, cmap=plt.cm.Greens)
plt.colorbar()
plt.show()
