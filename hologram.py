
# coding: utf-8
#!/usr/bin/python


# Algoritmos de cálculo de hologramas de fase p/ feixes gaussianos (versão Python)
# Sérgio R. Muniz  -- IFSC/USP  (2015)
# Baseado em método proposto por A.L. Gaunt & Z. Hadzibabic @DOI: 10.1038/srep00721
# Última alteração: 28/11/2016  -- by SRM
## DO NOT DISTRIBUTE (or publish) this code without permission!!


import numpy as np
import matplotlib.pyplot as plt


def GS(InputAmplitude, TargetAmplitude, Offset, SLMsize, Iterations):
    # --- Offset target amplitude (to remove "optical-vortices")
    TargetAmplitude = 0.8*TargetAmplitude + Offset
    #TargetAmplitude = np.sqrt(TargetAmplitude**2 + Offset**2)
    #     construct a new initial phase
    InitialPhase = MakeInitialPhaseGuess(np.shape(TargetAmplitude))

    # --- Model the SLM aperture using a mask
    ApertureMask = np.zeros( np.shape(TargetAmplitude)  )
    x1 = ( np.shape(TargetAmplitude)[0]-SLMsize[0] )//2 - 1  # for zero-indexed arrays
    x2 = ( np.shape(TargetAmplitude)[0]+SLMsize[0] )//2 - 1
    y1 = ( np.shape(TargetAmplitude)[1]-SLMsize[1] )//2 - 1
    y2 = ( np.shape(TargetAmplitude)[1]+SLMsize[1] )//2 - 1
    ApertureMask[x1:x2,y1:y2] = 1.0
    InputAmplitude = ParsevalScale(ApertureMask*InputAmplitude, TargetAmplitude)
    
    
    # --- FFT shifts
    InputAmplitude  = np.fft.fftshift(InputAmplitude)
    TargetAmplitude = np.fft.fftshift(TargetAmplitude)
    # --- Make OMRAF noise region mask
    Mask = np.fft.fftshift(ApertureMask)
    #Mask = 1.0
    # --- Prepare initial SLM plane
    SLMPlane = InputAmplitude * np.exp(1j * InitialPhase)
    # --- Mixing parameter
    mDM = 0.4
    #mDM = 1.0
    
    # --- Run Gerchberg Saxton algorithm
    for i in range(Iterations):
        ImageReconstruction = np.abs(np.fft.ifft2(SLMPlane))
        Amp =  (mDM*Mask*TargetAmplitude + (1-mDM)*(1-Mask)*ImageReconstruction)
        ImagePlane = Amp*np.exp(1j * np.angle(np.fft.ifft2(SLMPlane)))
        ImagePlane = np.sqrt(np.sum(TargetAmplitude**2))/np.sqrt(np.sum(ImagePlane**2)) * ImagePlane
        SLMPlane = InputAmplitude * np.exp(1j * np.angle(np.fft.fft2(ImagePlane)))

    # --- Return kinoform
    return np.fft.fftshift(SLMPlane)


def MakeInitialPhaseGuess(TargetSize):
    # --- Make initial parabolic phase pattern guess (*See MRAF's paper, by DeMarco's group)
    sx = (TargetSize[1])/2
    sy = (TargetSize[0])/2
    Xrange = np.arange(-sx,sx)
    Yrange = np.arange(-sy,sy)
    X, Y = np.meshgrid(Xrange,Yrange)
    a = 20.0
    b = 1.0*a
    return np.fft.fftshift( (X/b)**2 + (Y/a)**2 )


#########
## Supplementary functions
def ParsevalScale(InputAmplitude, TargetAmplitude):
    # --- Parseval scaling to conserve energy
    RMStarget = np.sqrt(np.mean(TargetAmplitude**2))
    RMSinput  = np.sqrt(np.mean( InputAmplitude**2))
    sx = np.shape(TargetAmplitude)[0]
    sy = np.shape(TargetAmplitude)[1]
    return ((RMStarget/RMSinput)*(sx*sy)**0.5)*InputAmplitude


def TheoreticalImagePlaneAmplitude(SLMPlane):
    # --- Reconstruct image plane from SLM plane
    return np.fft.fftshift(np.fft.ifft2(SLMPlane))

def process(initial, target):
    if np.ndim(initial) > 2: initial = initial[:, :, 0]
    if np.ndim(target) > 2: target = target[:, :, 0]
    SLM = TheoreticalImagePlaneAmplitude(GS(initial, target, 1, (512, 640), 30))
    hologram  = np.angle(SLM)
    intensity = np.abs(SLM)
    return hologram, intensity


#InitialAmplitude = plt.imread(input("Initial amplitude: "))
#TargetAmplitude  = plt.imread(input("Target amplitude: "))
#Offset           = int(input("Offset (1): ") or 1)
#Iterations       = int(input("Iterations (30): ") or 30)
#SLMsize          = int(input("SLMxsize (512): ") or 512), int(input("SLMysize (640): ") or 640)

#if np.ndim(InitialAmplitude) > 2: InitialAmplitude = InitialAmplitude[:, :, 0]
#if np.ndim(TargetAmplitude) > 2:  TargetAmplitude = TargetAmplitude[:, :, 0]

#Eslm = GS(InitialAmplitude, TargetAmplitude, Offset, SLMsize, Iterations)
#AEslm = TheoreticalImagePlaneAmplitude(Eslm)

#phase     = np.angle(AEslm)
#intensity = np.abs(AEslm)

#plt.imsave("processed/phase.png", phase)

#plt.figure()
#plt.imshow(intensity)
#plt.colorbar()
#plt.title("Imagem calculada")
#plt.imsave("processed/intensity.png", intensity)
