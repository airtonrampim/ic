import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import os
from glob import glob
from lib import mse, read_file

label = read_file('noise_output/label.txt')

target_original = plt.imread('noise_output/targets/%s_original.jpg' % label)
hologram_original = plt.imread('noise_output/holograms/%s_original.jpg' % label)
intensity_original = plt.imread('noise_output/intensities/%s_original.jpg' % label)

#Rates, MaxIntensities = [], []
TargetsRates, HologramsRates, IntensitiesRates = [], [], []
#TargetsMax, HologramsMax, IntensitiesMax = [], [], []
for path in glob('noise_output/targets/%s*.jpg' % label):
    path = os.path.basename(path)

    print(os.path.join('noise_output', 'targets', path))
    target_perturbed = plt.imread(os.path.join('noise_output', 'targets', path))
    hologram_perturbed = plt.imread(os.path.join('noise_output', 'holograms', path))
    intensity_perturbed = plt.imread(os.path.join('noise_output', 'intensities', path))

    target_error = mse(target_original, target_perturbed)
    hologram_error = mse(hologram_original, hologram_perturbed)
    intensity_error = mse(intensity_original, intensity_perturbed)

    name = os.path.splitext(path)[0]
    _, *values = name.split('_')

    if values[0] == 'rate':
        rate = float(values[1])
        TargetsRates.append([rate, target_error])
        HologramsRates.append([rate, hologram_error])
        IntensitiesRates.append([rate, intensity_error])
        #Rates.append(rate)
    #elif values[0] == 'max':
    #    TargetsMax.append(target_error)
    #    HologramsMax.append(hologram_error)
    #    IntensitiesMax.append(intensity_error)
    #    MaxIntensities.append(float(values[1]))

#Rates = np.array(Rates)
#MaxIntensities = np.array(MaxIntensities)
TargetsRates = np.array(TargetsRates)
HologramsRates = np.array(HologramsRates)
IntensitiesRates = np.array(IntensitiesRates)
#TargetsMax = np.array(TargetsMax)
#HologramsMax = np.array(HologramsMax)
#IntensitiesMax = np.array(IntensitiesMax)

#plt.clf()
#plt.xlabel('p')
#plt.ylabel('MSE$(p, 255)$')
#plt.plot(Rates, TargetsRates, 'o', label='Armadilha')
#plt.plot(Rates, HologramsRates, 'o', label='Holograma')
#plt.plot(Rates, IntensitiesRates, 'o', label='Intensidade')
#plt.legend()
#plt.savefig('noise_output/compare_rate.jpg')

df_targets = pd.DataFrame(data = TargetsRates, columns = ['rate', 'mse'])
df_targets.to_csv('noise_output/targets_rate.csv', index=False)
df_holograms = pd.DataFrame(data = HologramsRates, columns = ['rate', 'mse'])
df_holograms.to_csv('noise_output/holograms_rate.csv', index=False)
df_intensities = pd.DataFrame(data = IntensitiesRates, columns = ['rate', 'mse'])
df_intensities.to_csv('noise_output/intensities_rate.csv', index=False)


#plt.clf()
#plt.xlabel('c')
#plt.ylabel('MSE$(0.001, c)$')
#plt.plot(MaxIntensities, TargetsMax, 'o', label='Armadilha')
#plt.plot(MaxIntensities, HologramsMax, 'o', label='Holograma')
#plt.plot(MaxIntensities, IntensitiesMax, 'o', label='Intensidade')
#plt.legend()
#plt.savefig('noise_output/compare_max.jpg')

#df_targets = pd.DataFrame(data = TargetsMax, columns = ['mse'])
#df_targets.to_csv('noise_output/targets_max.csv', index=False)
#df_holograms = pd.DataFrame(data = HologramsMax, columns = ['mse'])
#df_holograms.to_csv('noise_output/holograms_max.csv', index=False)
#df_intensities = pd.DataFrame(data = IntensitiesMax, columns = ['mse'])
#df_intensities.to_csv('noise_output/intensities_max.csv', index=False)
