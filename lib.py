import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Rectangle
from sklearn.metrics import mean_squared_error

def mse(image1, image2):
    y1 = np.array(image1, dtype=np.float)
    y2 = np.array(image2, dtype=np.float)
    if y1.shape == y1.shape:
        return np.sum([mean_squared_error(y1[:,:,i], y2[:,:,i]) for i in range(y1.shape[2] - 1)])
    else:
        raise Exception('Tamanho das imagens sao diferentes')

def write_file(file_name, string):
    with open(file_name, 'w') as f: f.write(string)

def read_file(file_name):
    with open(file_name, 'r') as f: return f.read()

def perturb(data, rate, max_intensity = 255):
    mask = np.zeros_like(data)
    mask[np.nonzero(data)] = 1
    mask[:,:,-1] = 0
    result = np.abs(data - mask*np.random.choice(a = [0, max_intensity], p = [1 - rate, rate], size=data.shape))
    return np.array(result, dtype=data.dtype)

def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    Font: http://www.icare.univ-lille1.fr/wiki/index.php/How_to_convert_a_matplotlib_figure_to_a_numpy_array_or_a_PIL_image

    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    # draw the renderer
    fig.canvas.draw()
 
    # Get the RGBA buffer from the figure
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_argb(), dtype=np.uint8)
    buf.shape = (w, h, 4)
 
    # canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
    buf = np.roll(buf, 3, axis = 2)
    return buf

def draw_ellipse(origin, a, b):
    plt.clf()
    plt.figure(figsize=(10.67, 10.67), dpi=96)

    ax = plt.gca()
    ax.axis('off')
    patch=Ellipse(xy=origin, width=a, height=b, angle=0,
                edgecolor='white', facecolor='white')
    ax.add_patch(patch)
    ax.figure.set_facecolor('black')
    scale = max(a, b)
    ax.set(xlim=[-1.05*scale, 1.05*scale], ylim=[-1.05*scale, 1.05*scale])
    
    return fig2data(plt.gcf())

def draw_rect(origin, a, b):
    plt.clf()
    plt.figure(figsize=(10.67, 10.67), dpi=96)

    ax = plt.gca()
    ax.axis('off')
    patch=Rectangle(xy=origin, width=a, height=b, angle=0,
                edgecolor='white', facecolor='white')
    ax.add_patch(patch)
    ax.figure.set_facecolor('black')
    scale = max(a, b)
    ax.set(xlim=[-1.05*scale, 1.05*scale], ylim=[-1.05*scale, 1.05*scale])
    
    return fig2data(plt.gcf())
