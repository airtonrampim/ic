# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 10:03:58 2017

@author: Cora
"""
##esse programa leva em consideração as dimensões 16mm X 12mm do Slm
##aqui utilizouse a escala 640 pix = 16mm, ajustando com o w calculado do laser como
## aprox. 4.87 mm
# 8.75

w = 8.75 * (640./16)

import numpy as np
import matplotlib.pyplot as plt
from pylab import uint8

width, height = 1024, 1024
I, J = np.arange(width), np.arange(height)
I, J = np.meshgrid(I, J)
i0, j0 = width/2., height/2.

im = 255*np.exp(-((I-i0)**2.0 + (J-j0)**2.0)/(w**2.0))
im = uint8(im)

plt.imsave('beam.png', im, cmap = plt.cm.gray)