import numpy as np
import matplotlib.pyplot as plt
import hologram as h

from lib import perturb, draw_ellipse, write_file

beam = plt.imread('beam.png')


target, label = draw_ellipse((0, 0), 4, 4), 'circle'
hologram, intensity = h.process(beam, target)

plt.imsave('noise_output/targets/%s_original.jpg' % label, target)
plt.imsave('noise_output/holograms/%s_original.jpg' % label, hologram)
plt.imsave('noise_output/intensities/%s_original.jpg' % label, intensity)
write_file('noise_output/label.txt', label)
