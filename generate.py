import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import hologram as h

from lib import perturb, fig2data, draw_ellipse, draw_rect


beam = plt.imread('beam.png')

figures = [(draw_ellipse((0, 0), 4, 4), 'circle'),
 (draw_ellipse((0, 0), 6, 4), 'ellipseh1'),
 (draw_ellipse((0, 0), 10, 4), 'ellipseh2'),
 (draw_ellipse((0, 0), 4, 6), 'ellipsev1'),
 (draw_ellipse((0, 0), 4, 10), 'ellipsev2'),
 (draw_rect((-2, -2), 4, 4), 'square'),
 (draw_rect((-3, -2), 6, 4), 'recth1'),
 (draw_rect((-5, -2), 10, 4), 'recth2'),
 (draw_rect((-2, -3), 4, 6), 'rectv1'),
 (draw_rect((-2, -5), 4, 10), 'rectv2')]

I = np.arange(400)
np.random.shuffle(I)

Holograms = []
for i, n in enumerate(I):
    print('Processando a armadilha %d ...' % (i + 1))

    image, label = figures[n % 10]
    image = perturb(image, rate = 0.001, max_intensity = 153)
    image_id = 'im%03d' % (i + 1) 
    Holograms.append((image_id, label))
    
    hologram, intensity = h.process(beam, image)
    plt.imsave('targets/%s/%s.jpg' % (label, image_id), image)
    plt.imsave('holograms/%s/%s.jpg' % (label, image_id), hologram)
    plt.imsave('intensities/%s/%s.jpg' % (label, image_id), intensity)

df = pd.DataFrame(data = Holograms, columns = ['image_id', 'label'])
df.to_csv('labels.csv', index=False)
