import numpy as np
import matplotlib.pyplot as plt
import hologram as h

from lib import perturb, read_file

beam = plt.imread('beam.png')
label = read_file('noise_output/label.txt')
target_original = plt.imread('noise_output/targets/%s_original.jpg' % label)

MaxIntensities = np.array([26, 51, 102, 153, 204, 255])

for max_int in MaxIntensities:
    print('Processando a armadilha para a intensidade maxima de %d ...' % (max_int))
    
    target_perturbed = perturb(target_original, 0.001, max_int)
    hologram_perturbed, intensity_perturbed = h.process(beam, target_perturbed)

    plt.imsave('noise_output/targets/%s_max_%d.jpg' % (label, max_int), target_perturbed)
    plt.imsave('noise_output/holograms/%s_max_%d.jpg' % (label, max_int), hologram_perturbed)
    plt.imsave('noise_output/intensities/%s_max_%d.jpg' % (label, max_int), intensity_perturbed)
